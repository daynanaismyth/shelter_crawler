package crawler;

import com.google.gson.Gson;

public class JSONUtils {
	
	public static String getJSONValue(Object object){
		Gson gson = new Gson();
        String json = gson.toJson(object);
        System.out.println("json = " + json);
        return json;
	}
}
