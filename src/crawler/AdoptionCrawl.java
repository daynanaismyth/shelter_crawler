package crawler;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import com.opencsv.CSVReader;

import models.Media;
import models.Scope;
import models.User;

public class AdoptionCrawl {

	private static final String SHELTER_CSV = "C://Users//postgres//Downloads//shelters.csv";
	private static CSVReader reader;

	private static void readCSV() throws IOException {
		reader = new CSVReader(new FileReader(SHELTER_CSV), ',', '"', 1);
		int count = 0;
		String[] nextLine;
		while ((nextLine = reader.readNext()) != null && count < 5) {
			ArrayList<String> lineValues = new ArrayList<String>();
			if (nextLine != null) {
				lineValues.addAll(Arrays.asList(nextLine[0].split(",")));
				User shelter = CreateUserUtil.mapUserFromCSVData(nextLine);
				if(shelter != null){
					String accessToken = CreateUserUtil.signupShelter(shelter);
					System.out.println("Fetching current user...\n\n");
					User createdShelter = CreateUserUtil.getCurrentUser(accessToken);
					Scope shelterAlbum = UploadMediaWithDogUtil.getUserScope(createdShelter.getId(), accessToken);
					System.out.println(createdShelter.toString());
					List<WebElement> animalsAvailable = ShelterCrawl.getDogsForAdoptionByShelter(nextLine[IndexConstants.ADOPT_WEBSITE.getIndex()]);
					for(WebElement animal : animalsAvailable){
						Media media = UploadMediaWithDogUtil.buildMediaFromWebData(shelterAlbum, createdShelter, animal);
						Media created = UploadMediaWithDogUtil.postNewMedia(media, accessToken);
						System.out.println("New media: " + created);
					}
				}
			}
			count++;
		}
	}
	
	public static void main(String[] args) throws Exception {
		readCSV();
	}
}
