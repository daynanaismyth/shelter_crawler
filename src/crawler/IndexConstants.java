package crawler;

public enum IndexConstants {
	
	SHELTER_NAME(0),
	SHELTER_WEBSITE(1),
	SHELTER_ID(2),
	ADOPT_WEBSITE(3),
	LOCATION(4),
	CONTACT(5);
	
	private final int index;
	
	private IndexConstants(int index){
		this.index = index;
	}
	
	public int getIndex(){
		return index;
	}
}
