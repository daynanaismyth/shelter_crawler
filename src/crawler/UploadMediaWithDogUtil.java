package crawler;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.google.gson.Gson;

import models.Dog;
import models.Media;
import models.MediaData;
import models.Scope;
import models.ScopeResponse;
import models.User;

public class UploadMediaWithDogUtil {

	private static final String GET_USER_SCOPE_API = "http://127.0.0.1:8080/api/scopes?page=0&size=1&userid=";
	private static final String CREATE_NEW_MEDIA_API = "http://127.0.0.1:8080/api/media";
	
	public static Scope getUserScope(Long userId, String token) throws ClientProtocolException, IOException{
		String getURL = GET_USER_SCOPE_API + userId;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(getURL);
		get.setHeader("Authorization", token);
		get.setHeader("Accept-Language", "en-US,en;q=0.8");
		get.setHeader("Accept-Encoding", "gzip, deflate");
		get.setHeader("Content-type", "application/json");
		HttpResponse response = httpClient.execute(get);
		String responseString = new BasicResponseHandler().handleResponse(response);
		return extractScope(responseString);
	}
	
	public static Media postNewMedia(Media media, String token) throws ClientProtocolException, IOException{
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(CREATE_NEW_MEDIA_API);
		StringEntity postingString = new StringEntity(JSONUtils.getJSONValue(media));
		post.setEntity(postingString);
		post.setHeader("Content-type", "application/json");
		post.setHeader("Authorization", token);
		post.setHeader("Accept-Language", "en-US,en;q=0.8");
		post.setHeader("Accept-Encoding", "gzip, deflate");
		HttpResponse response = httpClient.execute(post);
		String responseString = new BasicResponseHandler().handleResponse(response);
		return extractMedia(responseString);
	}
	
	private static Media extractMedia(String responseString){
		Gson gson = new Gson();
		Media created = gson.fromJson(responseString, Media.class);
		return created;
	}
	
	public static Media buildMediaFromWebData(Scope scope, User owner, WebElement element) throws IOException{
		Media media = null;
		WebElement petData = element.findElement(By.className("adoptablePets-item"));
		Dog dog = buildDogFromWebData(petData);
		dog.setOwner(owner);
		if(petData != null){
			media = new Media();
			media.setOwner(owner);
			media.setScope(scope);
			MediaData retina = new MediaData();
			MediaData thumbnail = new MediaData();
			WebElement link = petData.findElement(By.tagName("a"));
			String dogImageGalleryLink = link.getAttribute("href").toString();
			String fileName = ShelterCrawl.getImageAndUpdateDogPropertiesCrawl(dogImageGalleryLink, dog);
			media.setDog(dog);
			List<Integer> widthAndHeight = getImageWidthAndHeight(fileName);
			retina.setFileName(fileName);
			retina.setResolution("RTN");
			retina.setWidth(widthAndHeight.get(0));
			retina.setHeight(widthAndHeight.get(1));
			thumbnail.setFileName(fileName);
			thumbnail.setResolution("TMB");
			thumbnail.setWidth(widthAndHeight.get(0));
			thumbnail.setHeight(widthAndHeight.get(1));
			media.setMediaType("IMAGE");	
			media.setRetina(retina);
			media.setThumbnail(thumbnail);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
			media.setShotTime(sdf.format(System.currentTimeMillis()));
		}
		return media;
	}
	
	public static Dog buildDogFromWebData(WebElement element){
		Dog dog = null;
		if(element != null){
			dog = new Dog();
			setDogStats(element, dog);
		}
		return dog;
	}
	
	private static void setDogStats(WebElement element, Dog dog){
		
		WebElement specs = element.findElement(By.className("specs"));
		String [] allSpecs = specs.getText().split("•");
		dog.setGender(allSpecs[1].trim());
 	}
	
	public static List<Integer> getImageWidthAndHeight(String fileName) throws IOException{
		List<Integer> widthAndHeight = new ArrayList<Integer>();
		if(fileName != null){
			URL url =new URL(fileName);
			BufferedImage image = ImageIO.read(url);
			int height = image.getHeight();
			int width = image.getWidth();
			widthAndHeight.add(width);
			widthAndHeight.add(height);
		}
		return widthAndHeight;
	}
	
	private static Scope extractScope(String responseString){
		Gson gson = new Gson();
		Scope scope = null;
		ScopeResponse scopeResponse = gson.fromJson(responseString, ScopeResponse.class);
		if(!scopeResponse.getData().isEmpty()){
			scope = scopeResponse.getData().get(0);
		}
		return scope;
	}
}
