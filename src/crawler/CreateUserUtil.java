package crawler;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.Gson;

import models.Location;
import models.TokenResponse;
import models.User;

public class CreateUserUtil {
	
	private static final String ACCOUNT_SIGNUP_API = "http://127.0.0.1:8080/api/signup?permission=true";
	private static final String GET_CURRENT_USER = "http://127.0.0.1:8080/api/me";
	private static final String SHELTER_ROLE = "SHELTER";
	private static final String WEB_CLIENT = "Basic ZHJlYW1kb2ctd2ViOmFSYmF6MjlkdmlCJWJkZDBwMTZ0";
	private static final String BEARER = "Bearer ";

	public static User mapUserFromCSVData(String[] row){
		User shelter = null;
		if(row.length > 0){
			shelter = new User();
			if(!row[(IndexConstants.CONTACT.getIndex())].contains("@")){
				return null;
			}
			//shelter.setEmail(row[(IndexConstants.CONTACT.getIndex())]);
			//shelter.setUsername(row[(IndexConstants.CONTACT.getIndex())]);
			shelter.setEmail("dayna@scopephotos.com");
			shelter.setUsername("dayna@scopephotos.com");
			shelter.setName(row[(IndexConstants.SHELTER_NAME.getIndex())]);
			shelter.setPassword("dreamdog");
			shelter.setLocation(new Location(row[(IndexConstants.LOCATION.getIndex())]));
			shelter.setRole(SHELTER_ROLE);
			
		}
		return shelter;
	}
	
	/**
	 * Post Request to sign up shelter
	 * @param shelter
	 * @return
	 * @throws IOException
	 */
	public static String signupShelter(User shelter) throws IOException{
		String postUrl = ACCOUNT_SIGNUP_API;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(postUrl);
		StringEntity postingString = new StringEntity(JSONUtils.getJSONValue(shelter));
		post.setEntity(postingString);
		post.setHeader("Content-type", "application/json");
		post.setHeader("Authorization", WEB_CLIENT);
		post.setHeader("Accept-Language", "en-US,en;q=0.8");
		post.setHeader("Accept-Encoding", "gzip, deflate");
		HttpResponse response = httpClient.execute(post);
		String responseString = new BasicResponseHandler().handleResponse(response);
		return extractToken(responseString);
	}
	
	public static User getCurrentUser(String token) throws ClientProtocolException, IOException{
		String getURL = GET_CURRENT_USER;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(getURL);
		get.setHeader("Authorization", token);
		get.setHeader("Accept-Language", "en-US,en;q=0.8");
		get.setHeader("Accept-Encoding", "gzip, deflate");
		get.setHeader("Content-type", "application/json");
		HttpResponse response = httpClient.execute(get);
		String responseString = new BasicResponseHandler().handleResponse(response);
		return extractUser(responseString);
	}
	
	private static String extractToken(String response){
		Gson gson = new Gson();
		TokenResponse token = gson.fromJson(response, TokenResponse.class);
		String accessToken = token.getToken().getAccessToken();
		System.out.println("Access token is: " + accessToken);
		return BEARER + accessToken;
	}
	
	private static User extractUser(String responseString){
		Gson gson = new Gson();
		User shelter = gson.fromJson(responseString, User.class);
		return shelter;
	}	
}
