package crawler;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.Proxy.ProxyType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import com.opencsv.CSVWriter;

import models.Dog;

/**
 * Initial PetFinder crawl to retrieve all shelters and their information
 * This data will be used to create Shelters and their adoptable pets for Dreamdog
 * @author postgres
 *
 */
public class ShelterCrawl {
	
	public static final String PETFINDER_SEARCH_SHELTER_URL = "https://www.petfinder.com/animal-shelters-and-rescues/search?page_number=";
	public static final String DRIVER_LOCATION = "C:\\Users\\postgres\\geckodriver.exe";
	public static final String PHANTOM_JS_LOCATION = "C:\\Users\\postgres\\phantomjs\\bin\\phantomjs.exe";
	public static final String CSV_LOCATION = "shelters.csv";
	public static CSVWriter writer;
	public static final int MAX_NUM_PAGES = 546 ;
	public static final int START_PAGE_NUM = 0;
	public static final String PROXY_HTTP = "104.196.243.162";
	public static final int PROXY_PORT = 80;
	public static final String PROXY_TYPE = "http";
	
	public static void crawlShelters() throws IOException {
		WebDriver driver = getConfiguredWebDriver();
		// Load the search shelter page
		for (int pageNum = START_PAGE_NUM; pageNum <= MAX_NUM_PAGES; pageNum++) {
			
			driver.get(PETFINDER_SEARCH_SHELTER_URL + pageNum);

			WebElement tableContainer = driver.findElement(By.id("js-sheltersSearch-resultsContainer"));
			List<WebElement> tableRows = tableContainer.findElements(By.xpath("tr"));
			for (WebElement we : tableRows) {
				List<WebElement> tableData = we.findElements(By.xpath("td"));
				List<String> csvData = new ArrayList<String>();
				for (WebElement data : tableData) {
					if (data.getAttribute("innerHTML").contains("<a href=")) {
						WebElement link = data.findElement(By.tagName("a"));
						String href = link.getAttribute("href").toString();
						String shelterId = extractId(href);
						if(shelterId != null){
							csvData.add(shelterId);
						}
						if(hasShelterName(href)){
							csvData.add(link.getText());
						}
						
						System.out.println("Link attribute" + link.getAttribute("href"));
						String emailAddress = extractEmailAddress(href);
						if(emailAddress != null){
							csvData.add(emailAddress);
						} else{
							csvData.add(href);
						}
					} else {
						csvData.add(data.getText());
					}
				}
				writeToCSV(csvData);
				csvData.clear();
			}
		}

	}
	
	public static WebDriver getConfiguredWebDriver(){
		FirefoxProfile fp = new FirefoxProfile();
		fp.setPreference("network.proxy.type", 1);
		fp.setPreference("network.proxy.http", PROXY_HTTP);
		fp.setPreference("network.proxy.http_port", PROXY_PORT);
				
		System.setProperty("webdriver.gecko.driver", DRIVER_LOCATION);
		WebDriver driver = new FirefoxDriver(null, fp);
//		DesiredCapabilities caps = DesiredCapabilities.phantomjs();
//		ArrayList<String> cliArgsCap = new ArrayList<String>();
//		//cliArgsCap.add("--proxy=" + PROXY_HTTP + ":" + PROXY_PORT);
//		//cliArgsCap.add("--proxy-type=" + PROXY_TYPE);
//		cliArgsCap.add("--ssl-protocol=tlsv1");
//		cliArgsCap.add("--ignore-ssl-errors=true");
//		cliArgsCap.add("--web-security=false");
//		cliArgsCap.add("--webdriver-loglevel=ALL");
//		caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS, cliArgsCap);
//		caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, PHANTOM_JS_LOCATION);
//		WebDriver driver = new PhantomJSDriver(caps);
		return driver;
	}
	
	public static void testProxy(){
		WebDriver driver = getConfiguredWebDriver();
		driver.get("http://whatismyipaddress.com/");
		System.out.println("Current address" + driver.getPageSource());
	}
	
	public static List<WebElement> getDogsForAdoptionByShelter(String availableAdoptionsWebpage){
		WebDriver driver = getConfiguredWebDriver();
		driver.get(availableAdoptionsWebpage);
		Select select = new Select(driver.findElement(By.id("fap-type")));
		select.selectByVisibleText("Dog");
		WebElement listOfAdoptions = driver.findElement(By.id("search-results"));
		List<WebElement> listItems = listOfAdoptions.findElements(By.xpath("li"));
		for(WebElement elm : listItems){
			System.out.println(elm.getText());
		}
		return listItems;
	}
	
	public static String getImageAndUpdateDogPropertiesCrawl(String animalGalleryWebpage, Dog dog) throws IOException{
		WebDriver driver = getConfiguredWebDriver();
		driver.get(animalGalleryWebpage);
		String description = driver.findElement(By.xpath("//meta[@property='og:description']")).getAttribute("content").toString();
		dog.setDescription(description);
		String name = driver.findElement(By.xpath("//*[@data-id='name']")).getText();
		dog.setName(name);
		WebElement animalGalleryList = driver.findElement(By.className("nrd-slide-media"));
		WebElement image = animalGalleryList.findElement(By.tagName("img"));
		System.out.println(image.getAttribute("src"));
		return image.getAttribute("src");
	}
	
	private static String extractId(String link){
		if(link.contains("shelter_id")){
			int index = link.indexOf("shelter_id") + "shelter_id".length() + 1;
			return link.substring(index);
		}
		return null;
	}
	
	private static boolean hasShelterName(String link){
		if(link.contains("/shelters/")){
			return true;
		}
		return false;
	}
	
	private static String extractEmailAddress(String link){
		if(link.contains("mailto:")){
			int index = link.indexOf("mailto:") + "mailto:".length();
			return link.substring(index);
		}
		return null;
	}
	
	private static void writeToCSV(List<String> csvData) throws IOException{	        
	      //Create record
		  csvData.add("\n");
	      String [] record = new String[csvData.size()];
	      record = csvData.toArray(record);
	      //Write the record to file
	      writer.writeNext(record);
	}
	
	private static void initializeCSVWriter() throws IOException{
		writer = new CSVWriter(new FileWriter(CSV_LOCATION, true));
	}
	
	public static void main(String[] args) throws Exception {
		testProxy();
		//initializeCSVWriter();
		//crawlShelters();
	    //writer.close();
		// Close the writer
    }
}
