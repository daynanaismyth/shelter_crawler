package models;

public class Dog {
	
	private String name;
	private String dogStatus;
	private String gender;
	private String size;
	private String houseTrained;
	private String specialNeeds;
	private String animalId;
	private String description;
	private String age;
	private String avatar;
	private User owner;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDogStatus() {
		return dogStatus;
	}
	public void setDogStatus(String dogStatus) {
		this.dogStatus = dogStatus;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getHouseTrained() {
		return houseTrained;
	}
	public void setHouseTrained(String houseTrained) {
		this.houseTrained = houseTrained;
	}
	public String getSpecialNeeds() {
		return specialNeeds;
	}
	public void setSpecialNeeds(String specialNeeds) {
		this.specialNeeds = specialNeeds;
	}
	public String getAnimalId() {
		return animalId;
	}
	public void setAnimalId(String animalId) {
		this.animalId = animalId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	public User getOwner(){
		return owner;
	}
	public void setOwner(User owner){
		this.owner = owner;
	}
	
}
