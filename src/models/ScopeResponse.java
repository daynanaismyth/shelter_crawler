package models;

import java.util.List;

public class ScopeResponse {
	// Data envelope
	private List<Scope> data;
	
	public List<Scope> getData(){
		return data;
	}
	
	public void setData(List<Scope> data){
		this.data = data;
	}
}
