package models;

import java.util.Date;

public class Media {
	
	private User owner;
	private Scope scope;
	private MediaData thumbnail;
	private MediaData retina;
	private Dog dog;
	private String mediaType;
	private String shotTime;
	
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public Scope getScope() {
		return scope;
	}
	public void setScope(Scope scope) {
		this.scope = scope;
	}
	public MediaData getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(MediaData thumbnail) {
		this.thumbnail = thumbnail;
	}
	public MediaData getRetina() {
		return retina;
	}
	public void setRetina(MediaData retina) {
		this.retina = retina;
	}
	public Dog getDog(){
		return dog;
	}
	public void setDog(Dog dog){
		this.dog = dog;
	}
	public String getMediaType(){
		return mediaType;
	}
	public void setMediaType(String mediaType){
		this.mediaType = mediaType;
	}
	public String getShotTime(){
		return shotTime;
	}
	public void setShotTime(String shotTime){
		this.shotTime = shotTime;
	}
}
