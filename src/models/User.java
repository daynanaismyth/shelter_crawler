package models;

public class User {
	
	private Long id;
	
	private String username;

    private String password;
    
    private String email;

    private String name;
	
	private String role;
	
	private Location location;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	public Location getLocation(){
		return location;
	}
	
	public void setLocation(Location location){
		this.location = location;
	}
	
	public Long getId(){
		return id;
	}
	
	public void setUserId(Long id){
		this.id = id;
	}
	
	@Override
	public String toString(){
		String s = "[Username: " + username + "]\n [Password: " + password + "]\n [Email: " + email + "]\n" +
				"[Name: " + name + "]\n [Role: " + role + "]\n [City: fix this"  +"]\n\n";
		return s;
	}
	
}
